# Version 1.0.0

- Now truncates polynomials to remove high degree terms that are zero.
