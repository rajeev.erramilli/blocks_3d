(* ::Package:: *)

highPrecisionToString[x_] := Module[
    {digits = 32, padding = 10, s},
    s = StringTrim[
        StringInsert[
            StringJoin[
                Sequence[Table["0", {i, digits + 1}]],
                ToString[
                    Quotient[Round[10^(digits + padding) Abs[x]], 10^padding]]],
            ".", -(digits + 1)],
        "0" ..];
    If[s == ".", "0",
       StringJoin[
           If[x < 0, "-", ""], 
           If[StringTake[s, 1] == ".",
              StringJoin["0", s],
              s]]]];

error[msg_] := (Print[msg]; Exit[];);

(* Fail with an error message if a file doesn't exist *)
safeGet[file_] := Check[
    If[FileExistsQ[file],
       Get[file],
       error[file <> " does not exist. Exiting."]],
    error["Error occurred getting " <> file <> ". Exiting."]];

(* Fail with an error message if a file doesn't exist *)
safeImport[file_] := Check[
    If[FileExistsQ[file],
       Import[file],
       error[file <> " does not exist. Exiting."]],
    error["Error occurred importing " <> file <> ". Exiting."]];

safeExport[file_, expr_] := Module[
    {dir = DirectoryName[file]},
    If[!DirectoryQ[dir],
       If[ $OperatingSystem == "Unix",
           Run["mkdir -p -m 777 "<>dir];,
           CreateDirectory[dir,CreateIntermediateDirectories->True];
       ];
    ];
    Export[file, expr];
    If[ $OperatingSystem == "Unix",
        Run["chmod a+rw "<>file];
    ];
];


(*
GetRelative[file] loads a file, with path provided relative to 
the file from which it is being called.
*)

GetRelative[file_]:=safeGet[FileNameJoin[{DirectoryName[$InputFileName],file}]];


(* The default working precision *)
prec=200;

(* highPrecision[var] guarantees that precision of var is at least prec *)
(* Mathematica thing, shouldn't be relevant for C++ code *)
highPrecision[var_]:=If[Precision[var]<prec,SetPrecision[var,prec],var];


twojsToString[twojs_]:=StringJoin@@(ToString/@twojs);


blockTypeStructDir[tableDir_, twojs_, q4pmstruct_]:=FileNameJoin[{
	tableDir,
	StringJoin["spins-",   twojsToString[twojs]],
	StringJoin["struct-",  twojsToString[q4pmstruct[[1]]+twojs], "-", If[q4pmstruct[[2]]==1,"plus","minus"]]
	}];


blockLogRhoLambdaTablePath[tableDir_, twojs_, delta12_, delta43_, twoj12_, twoj43_, q4pmstruct_, twoj_, rders_, n_, keptPoleOrder_, order_] := FileNameJoin[{
    blockTypeStructDir[tableDir,twojs,q4pmstruct],
    "partial",
    StringJoin["logRhoLambdaDerivs",
    "-spins-",             twojsToString[twojs],
    "-struct-",            twojsToString[q4pmstruct[[1]]+twojs], "-", If[q4pmstruct[[2]]==1,"plus","minus"],
    "-delta12-",           highPrecisionToString[delta12],
    "-delta43-",           highPrecisionToString[delta43],
    "-twoj12-",            ToString[twoj12],
    "-twoj43-",            ToString[twoj43],
    "-twoj",               ToString[twoj],
    "-rders",              ToString[rders],
    "-lambdader-",          ToString[n],
    "-keptPoleOrder",      ToString[keptPoleOrder],
    "-order",              ToString[order],
    ".m"]
    }];


blockTablePath[tableDir_, vars_,twojs_, delta12_, delta43_, twoj12_, twoj43_, q4pmstruct_, twoj_, lambda_, keptPoleOrder_, order_] := FileNameJoin[{
    blockTypeStructDir[tableDir,twojs,q4pmstruct],
    "partial",
    StringJoin[vars,
    "-spins-",             twojsToString[twojs],
    "-struct-",            twojsToString[q4pmstruct[[1]]+twojs], "-", If[q4pmstruct[[2]]==1,"plus","minus"],
    "-delta12-",           highPrecisionToString[delta12],
    "-delta43-",           highPrecisionToString[delta43],
    "-twoj12-",            ToString[twoj12],
    "-twoj43-",            ToString[twoj43],
    "-twoj",               ToString[twoj],
    "-Lambda",              ToString[lambda],
    "-keptPoleOrder",      ToString[keptPoleOrder],
    "-order",              ToString[order],
    ".m"]
    }];
