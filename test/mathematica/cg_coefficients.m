(* ::Package:: *)

(*

This file contains functions which implement general and specialized CG coefficients

*)


(*

This function is equivalent to ClebschGordan[{j1,m1},{j2,m2},{J,M}].
It has been implemented mainly because ClebschGordan in Mathematica is slow.
In C++ implementation it might make sense to use a package, if it is significantly faster
than this implementation.

Valid input is when 
IntegerQ[J-j1-j2]&&(Abs[j1-j2]<=J<=j1+j2)&&
IntegerQ[J-M]&&IntegerQ[j1-m1]&&IntegerQ[j2-m2]&&
(Abs[M]<=J)&&(Abs[m1]<=j1)&&(Abs[m2]<=j2)

It doesn't use "twoj" variables to be consistent with Mathematica's CG coeffs.
So the type signature is

({HalfInt, HalfInt},{HalfInt, HalfInt},{HalfInt, HalfInt})\[Rule]Real,

where HalfInt is a half-integer.

*)

CGFormula[{j1_, m1_}, {j2_, m2_}, {J_, M_}]:=
        If[IntegerQ[J-j1-j2] && (Abs[j1-j2]<=J<=j1+j2)
           && IntegerQ[J-M] && IntegerQ[j1-m1] && IntegerQ[j2-m2]
           && (Abs[M]<=J) && (Abs[m1]<=j1) && (Abs[m2]<=j2) && (M==m1+m2),
           Sqrt[((2J + 1) * (J + j1 - j2)! * (J - j1 + j2)! * (j1 + j2 - J)!)
                / (j1 +j2 + J + 1)!]
           * Sqrt[(J + M)! * (J - M)! * (j1 + m1)! * (j1 - m1)!
                  * (j2 + m2)! * (j2 - m2)!]
           * Sum[(-1)^k/(k! * (j1 + j2 - J - k)! * (j1 - m1 - k)!
                         * (j2 + m2 - k)! * (J - j2 + m1 + k)!
                         * (J - j1 - m2 + k)!),
                 {k, Max[0, -(J - j2 + m1), -(J - j1 - m2)],
                  Min[j1 + j2 - J, j1 - m1, j2 + m2]}],
           0];


(* 
This is an implementation of CG ceofficients that appear in the recursion matrices.

SpecialCG[j12,j120,j,m] is equal to ClebschGordan[{j12,-m},{j,m},{j120,0}]=CGFormula[{j12,-m},{j,m},{j120,0}] 

The input is valid when the corresponding input to CGFormula is valid.

The implementation uses a formula that is supposed to work well for our purposes, 
i.e. when j and j120 can be relatively large (~100) but j-j120 is smallish. 
In particular, Pochhammers and the sum are supposed to have only a few terms.

This function is called often.
In C++ implementation replace by package implementation only if package implementation is faster.

(HalfInt,HalfInt,HalfInt,HalfInt) \[Rule] Real

*)
SpecialCG[j12_, j120_, j_, m_]/;m>0:=(-1)^(j12 + j - j120) SpecialCG[j12, j120, j, -m];
SpecialCG[j12_, j120_, j_, m_]/;m<=0:=
        Module[{s=j120-j},
               If[IntegerQ[j120-j12-j] && (Abs[j12-j]<=j120<=j12+j)
                  && IntegerQ[j120] && IntegerQ[j12-m] && IntegerQ[j-m]
                  && (Abs[m]<=j12) && (Abs[m]<=j),
                  Sqrt[Pochhammer[j + m + 1, -2*m] * (j12 - m)! * (j12+m)!]
                  * Sqrt[((1 + 2*(j + s)) * (j12 - s)! * (j12 + s)!)
                         / Pochhammer[2*j - j12 + s + 1, 2*j12 + 1]]
                  * Sum[((-1)^k * Pochhammer[j + m - k + 1, k]
                         * Pochhammer[-j12 + j120 + k - m + 1, j12 + m - k])
                        / ((j + j12 - j120 - k)! * k!
                           * (-j + j120 + k - m)! * (j12-k+m)!),
                        {k, Max[0, j-j120+m, j12-j120+m],
                         Min[j12+m, j+m, j+j12-j120]}], 0]
]
