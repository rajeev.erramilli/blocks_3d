(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];


GetRelative["conventions.m"];


GetRelative["pole_classification.m"];
GetRelative["structure_properties.m"];


GetRelative["cg_coefficients.m"];


(* DPrefactor : (Int,(Family,Int)) \[Rule] Real *)
DPrefactor[twoj_,{family_,k_}]:=cNorm[poleDelta[twoj,{family,k}]+poleShift[{family,k}],poleTwoJ[twoj,{family,k}]]/cNorm[poleDelta[twoj,{family,k}],twoj];

(*
The below implements D_{i,m} functions in 3.3.2 of 1907.11247. 
To make everything real we took out factors like Sqrt[-1] from D_{i,m} and combined their squares into an additional residue factor Q
*)
Q[twoj_, {"I", k_}]:=-DPrefactor[twoj,{"I",k}];
Q[twoj_, {"II", k_}]:=-DPrefactor[twoj,{"II",k}] ;
Q[twoj_, {"III", k_}]:=-DPrefactor[twoj,{"III",k}] ((k)(1/2+twoj/2-k))/(1/2+twoj/2+k) ;
Q[twoj_, {"IV", k_}]:= DPrefactor[twoj,{"IV",k}] ((1+twoj/2-k)(2k-1))/(twoj+2k);

(* DNormalized : (Int,(Family,Int),Int) \[Rule] Real *)
DNormalized[twoj_,{"I",k_}][twom_]:=
If[1<=k,
Sqrt[1/(k!*(k-1)!)*(Pochhammer[1+(twoj-twom)/2,k]Pochhammer[1+(twoj+twom)/2,k])/Pochhammer[1+twoj,2*k]]
,0];

DNormalized[twoj_,{"II",k_}][twom_]:=
If[(Abs[twom]<=twoj-2k)&&(1<=k<=twoj/2),
Sqrt[1/(k!*(k-1)!) (Pochhammer[1+(twoj-twom)/2-k,k]Pochhammer[1+(twoj+twom)/2-k,k])/Pochhammer[2+twoj-2k,2k]]
,0];

DNormalized[twoj_,{"III",k_}][twom_]:=
If[(bosonic[twoj]&&(1<=k))||(fermionic[twoj]&&(Abs[twom]>=2k+1)&&(2<=2k<=twoj)),Pochhammer[-k+twom/2+1/2,2k]/((2k)!Pochhammer[-k+twoj/2+1/2,2k])
,0];

DNormalized[twoj_,{"IV",k_}][twom_]:=
If[(bosonic[twoj]&&(Abs[twom]>=2k)&&(2<=2k<=twoj))||(fermionic[twoj]&&(1<=k)),Pochhammer[1-k+twom/2,2k-1]/((2k-1)!Pochhammer[1+twoj/2-k,2k-1])
,0];

(* (Int,{Family,Int})\[Rule]Int *)

twomMin[twoj_,{"I",k_}]:=If[bosonic[twoj],0,1];
twomMin[twoj_,{"II",k_}]:=If[bosonic[twoj],0,1];
twomMin[twoj_,{"III",k_}]:=If[bosonic[twoj],0,2k+1];
twomMin[twoj_,{"IV",k_}]:=If[bosonic[twoj],2k,1];


(*F[\[CapitalDelta]_,twoj_]:=2\[Pi] (2^(3-\[CapitalDelta]) \[Pi]^(3/2))/(Gamma[(\[CapitalDelta]+twoj/2)/2]Gamma[(\[CapitalDelta]-1-twoj/2)/2]);*)
(*Using Pochhammers in Fratio[\[CapitalDelta],shift,twoj,twojp]=F[\[CapitalDelta],twoj]/F[\[CapitalDelta]+shift,twojp] better handles the situation where poles of the Gamma functions cancel against each other.*)

(* Fratio : (Real,Int,Int,Int) \[Rule] Real *)
Fratio[\[CapitalDelta]_,shift_,twoj_,twojp_]:=2^shift Pochhammer[1/2 (-1-twoj/2+\[CapitalDelta]),1/2 (shift-(twojp-twoj)/2)]Pochhammer[1/2 (twoj/2+\[CapitalDelta]),1/2 (shift+(twojp-twoj)/2)];
(* Here, the second argument of Pochhammer is always integer. This is for the following reason

Parity of a structure is determined by parity of j120-j. For parity even operator j shifts by the same parity as shift varible above, and so does j120. 
For parity-odd similar argument works.

*)

(*Note: need to fix typos in 4.2 of summary.pdf, where argument of F^-1 should be {\[CapitalDelta]i'+\[Delta],j'120} and argument of F should be {\[CapitalDelta]i+\[Delta],j120}.*)
(* M : ((Family,Int),Real,Int,Int,Int,Int) \[Rule] Real *)
ClearAll[M];
M[{family_,k_},\[Delta]_,twoj12_,twoj120_,twoj120p_,twoj_]:=
M[{family,k},\[Delta],twoj12,twoj120,twoj120p,twoj]=
Module[
	{twojp=poleTwoJ[twoj,{family,k}],
	 shift=poleShift[{family,k}],
	 Delta=poleDelta[twoj,{family,k}]
	},
	If[
		structureAllowed[twoj,{twoj12,twoj120}]&&
		structureAllowed[twojp,{twoj12,twoj120p}]&&
		EvenQ[(twoj120-twoj-(twoj120p-twojp))/2+poleParity[family]](* checks parity match *)
	,
		Sum[
				(2-KroneckerDelta[twom,0])*Fratio[Delta+\[Delta],shift,twoj120,twoj120p]*
				(*ClebschGordanSafe[{twoj12/2,-twom/2},{twojp/2,twom/2},{twoj120p/2,0}]**)
				SpecialCG[twoj12/2,twoj120p/2,twojp/2,twom/2]*
				DNormalized[twoj,{family,k}][twom]*
				(*ClebschGordanSafe[{twoj12/2,-twom/2},{twoj/2,twom/2},{twoj120/2,0}]*)
				SpecialCG[twoj12/2,twoj120/2,twoj/2,twom/2]
		,{twom,twomMin[twoj,{family,k}],Min[twoj12,twojp,twoj],2}] (* Summing only over m>0 and multiplying by (2-KroneckerDelta[m,0] *)
	,0]
];

(* LFunction : ((Family,Int),Real,Int,Int,Int,Int) \[Rule] Real *)
LFunction[{family_,k_},\[CapitalDelta]12_,twoj12_,twoj120_,twoj120p_,twoj_]:=M[{family,k},\[CapitalDelta]12,twoj12,twoj120,twoj120p,twoj];

(* RMatrix : ((Family,Int),Real,Int,Int,Int,Int) \[Rule] Real *)
RFunction[{family_,k_},\[CapitalDelta]43_,twoj43_,twoj430_,twoj430p_,twoj_]:=(-1)^((poleTwoJ[twoj,{family,k}]-twoj)/2) M[{family,k},\[CapitalDelta]43,twoj43,twoj430,twoj430p,twoj];


ClearAll[MMatrix];
MMatrix[{family_,k_},\[CapitalDelta]12_,twoj12_,primepseudoparity_,twoj_]:=
MMatrix[{family,k},\[CapitalDelta]12,twoj12,primepseudoparity,twoj]=
Module[
{
structures=so3TensorParity[Mod[primepseudoparity+poleParity[family],2],twoj12,twoj],
primestructures=so3TensorParity[primepseudoparity,twoj12,poleTwoJ[twoj,{family,k}]],
twoj120,twoj120p
},
Table[
M[{family,k},\[CapitalDelta]12,twoj12,twoj120,twoj120p,twoj]
,{twoj120,structures}
,{twoj120p,primestructures}]
];


LMatrix=MMatrix;
RMatrix[{family_,k_},\[CapitalDelta]43_,twoj43_,primepseudoparity_,twoj_]:=(-1)^((poleTwoJ[twoj,{family,k}]-twoj)/2)MMatrix[{family,k},\[CapitalDelta]43,twoj43,primepseudoparity,twoj];
