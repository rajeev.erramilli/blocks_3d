#!/bin/bash

# Scalar
./build/blocks_3d --j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0, 0, 0, 0" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0, 0, 0, 0" --four-pt-sign=1 --order 20 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"

# Scalar-fermion
./build/blocks_3d --j-external "0.5, 0, 0, 0.5" --j-internal "0.5-4.5" --j-12 0.5 --j-43 0.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0, 0, -0.5" --four-pt-sign "-1" --order 15 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0.5, 0, 0, 0.5" --j-internal "0.5-4.5" --j-12 0.5 --j-43 0.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0, 0, -0.5" --four-pt-sign "-1" --order 15 --lambda 5 --kept-pole-order 10 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

# Four-fermion
./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 0 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 0 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "0.5, 0.5, 0.5, 0.5" --j-internal "0-4" --j-12 1 --j-43 1 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "0.5, 0.5, 0.5, -0.5" --four-pt-sign "1" --order 10 --lambda 5 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

# Vector-fermion
./build/blocks_3d --j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, -0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, -0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"

./build/blocks_3d --j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, 0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "1, 0.5, 1, 0.5" --j-internal "0.5-4.5" --j-12 1.5 --j-43 1.5 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 0.5, 1, 0.5" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws,xt_radial,ws_radial"

# Vector-Vector
./build/blocks_3d --j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 1" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 1" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"

./build/blocks_3d --j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 0" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws" --delta-1-plus-2="2.3"
./build/blocks_3d --j-external "1, 1, 1, 1" --j-internal "0-4" --j-12 2 --j-43 2 --delta-12 0.12 --delta-43 0.543 --four-pt-struct "1, 1, 1, 0" --four-pt-sign "1" --order 7 --lambda 3 --kept-pole-order 5 --num-threads 4 --precision 665 -o test/benchmark/derivs_{}.json --coordinates="zzb,yyb,xt,ws"


