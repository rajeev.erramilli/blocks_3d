
* [Requirements](#requirements)
* [Installation](#installation)

# Requirements

Blocks_3D requires

- A modern C++ compiler with C++ 14 support.

- [Boost C++ Libraries](http://www.boost.org/) Versions other than
  1.68 and the latest 1.74 have a performance bug which prevents them
  from performing well with multiple cores.

- [The GNU Multiprecision Library](https://gmplib.org/) This is used
  by the Boost multiprecision library.  Only the C bindings are
  required.

- [Eigen](https://eigen.tuxfamily.org)

- [fmt](https://fmt.dev/latest/index.html)

- [Python](https://python.org)

Blocks_3D has only been tested on Linux (Debian buster and Centos 7
and 8).  In principle, it should be installable on Mac OS X using
libraries from a package manager such as [Homebrew](https://brew.sh).

Some of these dependencies may be available via modules or a package
manager.  On Yale's Grace cluster:

    module load git CMake GCCcore/6.4.0 Boost/1.66.0-foss-2018a GMP/6.1.2-GCCcore-6.4.0 Eigen/3.3.7

On Caltech's central HPC cluster:

    module load cmake/3.10.2 gcc/7.3.0 boost/1_68_0-gcc730 eigen/eigen

On Debian Buster:

    apt-get install libeigen3-dev libboost-dev libfmt-dev libgmp-dev

# Installation

1. Download Blocks_3D

        git clone https://gitlab.com/bootstrapcollaboration/blocks_3d.git
        cd blocks_3d

2. Configure the project using the included version of
   [waf](https://waf.io).  Waf can usually find libraries that are in
   system directories, but it needs direction for everything else is.
   If you are having problems, running `python ./waf --help` will give
   you a list of options.
   
   On Yale's Grace cluster, a working command with a locally built copy of fmt

        ./waf configure --prefix=$HOME/project/install --fmt-dir=$HOME/project/install --fmt-libdir=$HOME/project/install/lib64 --boost-dir=$BOOST_ROOT

    On Caltech's central HPC cluster, again with a locally built copy of fmt

        ./waf configure --prefix=$HOME/install --eigen-incdir=/software/eigen-b3f3d4950030/ --fmt-dir=$HOME/install --fmt-libdir=$HOME/install/lib64

    and on Debian buster, it is

        CXX=mpicxx python ./waf configure

3. Type `python ./waf` to build the executable in `build/blocks_3d`.  Running
   
        ./build/blocks_3d --help
   
   should give you a usage message.
