#include <array>
#include <cstdint>
#include <cstddef>
#include <cmath>

namespace
{
  int64_t four_point_parity(const std::array<int64_t, 4> &two_js,
                            const std::array<int64_t, 4> &q4)
  {
    int64_t sum(0);
    for(size_t index = 0; index < q4.size(); ++index)
      {
        sum += two_js[index] - q4[index];
      }
    return (sum / 2) % 2;
  }
}

std::array<std::array<int64_t, 2>, 2>
compute_pseudo_parity_types(const std::array<int64_t, 4> &two_js,
                            const std::array<int64_t, 4> &q4,
                            const std::array<int64_t, 3> &j12_terms,
                            const std::array<int64_t, 3> &j43_terms)
{
  std::array<std::array<std::array<int64_t, 2>, 2>, 2> parities;
  parities[0] = {std::array<int64_t, 2>({0, 0}), {1, 1}};
  parities[1] = {std::array<int64_t, 2>({0, 1}), {1, 0}};
  std::array<std::array<int64_t, 2>, 2> parity_types(
    parities[four_point_parity(two_js, q4)]);

  std::array<std::array<int64_t, 2>, 2> result;
  for(size_t column = 0; column < 2; ++column)
    {
      result[column][0]
        = std::abs((j12_terms[0] - j12_terms[1] - j12_terms[2]) / 2
                   + parity_types[column][0])
          % 2;
      result[column][1]
        = std::abs((j43_terms[0] - j43_terms[1] - j43_terms[2]) / 2
                   + parity_types[column][1])
          % 2;
    }
  return result;
}
