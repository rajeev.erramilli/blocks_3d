#include "Single_Spin.hxx"
#include "Q4pm.hxx"
#include "Coordinate.hxx"

#include <set>

void handle_input(const int &argc, char *argv[], std::string &delta_12_string,
                  std::string &delta_43_string,
                  std::string &delta_1_plus_2_string,
                  std::set<Coordinate> &coordinates, int64_t &lambda,
                  int64_t &order, int64_t &kept_pole_order, int64_t &two_j12,
                  int64_t &two_j43, std::array<int64_t, 4> &two_js,
                  std::set<int64_t> &two_js_intermediate, Q4pm &q4pm,
                  size_t &precision_base_2, size_t &num_threads,
                  std::string &output_template, bool &debug);

std::vector<std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>>
partial_tables(const std::array<int64_t, 4> &two_js, const Bigfloat &delta_12,
               const Bigfloat &delta_43, const int64_t &two_j12,
               const int64_t &two_j43, const Q4pm &q4pm,
               const std::set<int64_t> &two_js_intermediate,
               const int64_t &lambda, const int64_t &kept_pole_order,
               const int64_t &order, const size_t &num_threads,
               const bool &debug,
               std::vector<std::set<Bigfloat>> &output_poles);

void write_output(
  const std::string &output_template, const int64_t &parity,
  const std::array<int64_t, 4> &two_js,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const std::vector<std::set<Bigfloat>> &output_poles,
  const size_t &precision_base_2, const int &argc, char *argv[],
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial,
  const size_t &num_threads, const bool &debug);

int main(int argc, char *argv[])
{
  try
    {
      std::string delta_12_string, delta_43_string, delta_1_plus_2_string;
      int64_t lambda, order, kept_pole_order, two_j12, two_j43;
      std::array<int64_t, 4> two_js;
      std::set<int64_t> two_js_intermediate;
      Q4pm q4pm;
      std::set<Coordinate> coordinates;
      size_t precision_base_2, num_threads;
      std::string output_template;
      bool debug;

      handle_input(argc, argv, delta_12_string, delta_43_string,
                   delta_1_plus_2_string, coordinates, lambda, order,
                   kept_pole_order, two_j12, two_j43, two_js,
                   two_js_intermediate, q4pm, precision_base_2, num_threads,
                   output_template, debug);
      const Bigfloat delta_12(delta_12_string), delta_43(delta_43_string);

      std::vector<std::set<Bigfloat>> output_poles;
      auto partial(partial_tables(two_js, delta_12, delta_43, two_j12, two_j43,
                                  q4pm, two_js_intermediate, lambda,
                                  kept_pole_order, order, num_threads, debug,
                                  output_poles));
      int64_t parity((1 - q4pm.i) / 2);

      write_output(output_template, parity, two_js, two_js_intermediate,
                   two_j12, two_j43, delta_12_string, delta_43_string,
                   delta_1_plus_2_string, q4pm, order, lambda, coordinates,
                   kept_pole_order, output_poles, precision_base_2, argc, argv,
                   partial, num_threads, debug);
    }
  catch(std::exception &e)
    {
      std::cerr << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cerr << "Unknown error\n";
      exit(1);
    }
}
