#pragma once

#include "../Bigfloat.hxx"

#include <vector>

struct Factorial
{
  std::vector<Bigfloat> cache, inverse_cache;

  // Initialize with 0! == cache[0] = 1
  Factorial() : cache(1, Bigfloat(1)), inverse_cache(1, Bigfloat(1)) {}
  // resizing can invalidate references, so pass Bigfloat
  Bigfloat eval(const int64_t &index)
  {
    if(static_cast<size_t>(index) >= cache.size())
      {
        grow_cache(index + 1);
      }
    return cache[index];
  }
  Bigfloat inverse_eval(const int64_t &index)
  {
    if(static_cast<size_t>(index) >= cache.size())
      {
        grow_cache(index + 1);
      }
    return inverse_cache[index];
  }
  void grow_cache(const int64_t &new_size)
  {
    const int64_t old_size(cache.size());
    cache.resize(new_size);
    inverse_cache.resize(new_size);
    for(int64_t index = old_size; index != new_size; ++index)
      {
        cache[index] = index * cache[index - 1];
        inverse_cache[index] = 1 / cache[index];
      }
  }
};
