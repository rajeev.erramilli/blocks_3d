#include <cstdint>
#include <vector>

// The values of 2*j for which we need to compute blocks at r_order-th
// order in r
std::vector<int64_t>
two_j_values(const int64_t &r_order, const int64_t &order,
             const int64_t &two_j12, const int64_t &two_j_max)
{
  std::vector<int64_t> result;
  for(int64_t v(two_j12 % 2); v <= two_j_max + 2 * (order - r_order); v += 2)
    {
      result.push_back(v);
    }
  return result;
}
