#include "dot.hxx"
#include "../../../../../Bigfloat.hxx"

namespace
{
  std::vector<Bigfloat> one_minus_r_squared(const int64_t &order)
  {
    std::vector<Bigfloat> result;
    for(int64_t n = 0; n <= order; ++n)
      {
        result.push_back(n % 2 == 0 ? (Pochhammer(Bigfloat(0.5), n / 2)
                                       / Pochhammer(Bigfloat(1), n / 2))
                                    : Bigfloat(0));
      }
    return result;
  }

  // This truncates to the order of the smaller series.
  std::vector<Bigfloat>
  power_series_times(std::vector<Bigfloat> &a, std::vector<Bigfloat> &b)
  {
    std::vector<Bigfloat> result;
    for(size_t n = 0; n < std::min(a.size(), b.size()); ++n)
      {
        Bigfloat sum(0);
        for(size_t m = 0; m <= n; ++m)
          {
            sum += a[m] * b[n - m];
          }
        result.push_back(sum);
      }
    return result;
  }
}

std::vector<std::vector<Bigfloat>>
prefactor_derivatives(const int64_t &order,
                      const std::vector<std::vector<Bigfloat>> &series,
                      const Bigfloat &c, const int64_t &k_max)
{
  std::vector<std::vector<Bigfloat>> result;
  std::vector<Bigfloat> one_m_r2(one_minus_r_squared(order));
  for(int64_t k = 0; k <= k_max; ++k)
    {
      std::vector<Bigfloat> series_table;
      for(int64_t n = 0; n <= order; ++n)
        {
          std::vector<Bigfloat> temp;
          for(int64_t j = 0; j <= n; ++j)
            {
              temp.push_back(
                (2 * j - n + 2 * c == 0 && k == 0)
                  ? Bigfloat(1)
                  : pow(2 * j - n + 2 * c, k));
            }
          series_table.push_back(dot(series.at(n), temp));
        }
      // TODO: do this in-place?
      result.push_back(power_series_times(series_table, one_m_r2));
    }
  return result;
}
