#include "../../../SO3.hxx"
#include "../../../../../../Cache.hxx"

#include <vector>

Bigfloat
f_W_conversion_matrix(const std::array<int64_t, 4> &two_js,
                      const int64_t &two_j, const SO3 &left_SO3,
                      const SO3 &right_SO3,
                      const std::array<int64_t, 3> &left_q3,
                      const std::array<int64_t, 3> &right_q3, Cache &cache);

Bigfloat f_q_basis_derivative(const std::array<int64_t, 4> &two_js,
                              const int64_t &two_j,
                              const std::array<int64_t, 3> &left_q3,
                              const std::array<int64_t, 3> &right_q3,
                              const std::array<int64_t, 4> &q4,
                              const int64_t &n, Factorial &factorial,
                              Sinh_Power_Derivative &sinh_power_derivative);

Bigfloat
f_SO3_basis_derivative(const std::array<int64_t, 4> &two_js,
                       const int64_t &two_j, const SO3 &left_SO3,
                       const SO3 &right_SO3, const std::array<int64_t, 4> &q4,
                       const int64_t &n, Cache &cache)
{
  std::vector<std::array<int64_t, 3>> left_q3_set;
  for(int64_t two_p1(-two_js[0]); two_p1 <= two_js[0]; two_p1 += 2)
    for(int64_t two_p2(-two_js[1]); two_p2 <= two_js[1]; two_p2 += 2)
      {
        int64_t two_p(-two_p1 - two_p2);
        if(std::abs(two_p) <= two_j)
          {
            left_q3_set.push_back({two_p1, two_p2, two_p});
          }
      }

  std::vector<std::array<int64_t, 3>> right_q3_set;
  // Note that the order of these two loops is switched from left_q3_set.
  for(int64_t two_p4(-two_js[3]); two_p4 <= two_js[3]; two_p4 += 2)
    for(int64_t two_p3(-two_js[2]); two_p3 <= two_js[2]; two_p3 += 2)
      {
        int64_t two_p(-two_p4 - two_p3);
        if(std::abs(two_p) <= two_j)
          {
            right_q3_set.push_back({two_p4, two_p3, two_p});
          }
      }

  Bigfloat result(0);
  for(auto &left_q3 : left_q3_set)
    for(auto &right_q3 : right_q3_set)
      {
        result += f_W_conversion_matrix(two_js, two_j, left_SO3, right_SO3,
                                        left_q3, right_q3, cache)
                  * f_q_basis_derivative(two_js, two_j, left_q3, right_q3, q4,
                                         n, cache.factorial,
                                         cache.sinh_power_derivative);
      }
  return result;
}
