#include "../../../SO3.hxx"
#include "../../../../clebsch_gordan.hxx"
#include "../../../../../../Cache.hxx"

#include <vector>

Bigfloat f_W_conversion_matrix(const std::array<int64_t, 4> &two_js,
                               const int64_t &two_j, const SO3 &left_SO3,
                               const SO3 &right_SO3,
                               const std::array<int64_t, 3> &left_q3,
                               const std::array<int64_t, 3> &right_q3,
                               Cache &cache)
{
  const int64_t two_j12(left_SO3[0]), two_j120(left_SO3[1]),
    two_j43(right_SO3[0]), two_j430(right_SO3[1]), two_q(left_q3[2]),
    two_qp(right_q3[2]);
  const std::array<int64_t, 4> two_qs(
    {left_q3[0], left_q3[1], right_q3[1], right_q3[0]});

  const int64_t sign(
    ((two_js[0] + two_js[3] - 2 * two_j + two_qs[1] + two_qs[2]) / 2) % 2 == 0
      ? 1
      : -1);

  Bigfloat product(1);
  for(int64_t ii = 0; ii < 4; ++ii)
    {
      product
        *= binomial_coefficient(two_js[ii], (two_js[ii] + two_qs[ii]) / 2);
    }

  return sign
         * clebsch_gordan(two_js[0], two_qs[0], two_js[1], two_qs[1], two_j12,
                          -two_q, cache.factorial)
         * cache.clebsch_gordan_eval(two_j12, two_j120, two_j, two_q)
         * clebsch_gordan(two_js[3], two_qs[3], two_js[2], two_qs[2], two_j43,
                          -two_qp, cache.factorial)
         * cache.clebsch_gordan_eval(two_j43, two_j430, two_j, two_qp)
         * sqrt(binomial_coefficient(two_j, (two_j + two_q) / 2)
                * binomial_coefficient(two_j, (two_j + two_qp) / 2) * product);
}
