#include "../../../../../../../../Bigfloat.hxx"
#include "../../../../../../../Factorial.hxx"
#include "../../../../../../../Sinh_Power_Derivative.hxx"

Bigfloat
wjm_matrix(const int64_t &two_j, const int64_t &two_m1, const int64_t &two_m2);

Bigfloat tilde_wigner_derivative(const int64_t &two_j, const int64_t &two_m,
                                 const int64_t &two_mp, const int64_t &Nn,
                                 Factorial &factorial,
                                 Sinh_Power_Derivative &sinh_power_derivative);

Bigfloat f_q_basis_derivative(const std::array<int64_t, 4> &two_js,
                              const int64_t &two_j,
                              const std::array<int64_t, 3> &left_q3,
                              const std::array<int64_t, 3> &right_q3,
                              const std::array<int64_t, 4> &q4,
                              const int64_t &n, Factorial &factorial,
                              Sinh_Power_Derivative &sinh_power_derivative)
{
  const std::array<int64_t, 4> two_ps(
    {left_q3[0], left_q3[1], right_q3[1], right_q3[0]});

  // We use F43 instead of F430 to have a real block.
  const int64_t two_p(left_q3[2]), two_pp(right_q3[2]),
    f43(two_js[3] % 2 + two_js[2] % 2);

  constexpr int64_t two_point_b(1);

  // The expression
  //
  // f43 - two_js[2] - two_js[3] - f0 = f43 - two_js[2] - two_js[3]
  //
  // is always even.

  const int64_t sign((f43 - two_js[2] - two_js[3]) % 4 == 0 ? 1 : -1);
  return sign * wjm_matrix(two_js[0], two_ps[0], q4[0])
         * wjm_matrix(two_js[1], two_ps[1], q4[1])
         * wjm_matrix(two_js[2], -two_ps[2], q4[2])
         * wjm_matrix(two_js[3], -two_ps[3], q4[3])
         * tilde_wigner_derivative(two_j, -two_pp, -two_p, n, factorial,
                                   sinh_power_derivative)
         / (two_point_b
            * sqrt(binomial_coefficient(two_j, (two_j - two_p) / 2)
                   * binomial_coefficient(two_j, (two_j - two_pp) / 2)));
}
