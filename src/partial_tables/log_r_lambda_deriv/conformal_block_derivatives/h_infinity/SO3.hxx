#pragma once

#include "../../../../ostream_array.hxx"

struct SO3
{
  std::array<int64_t,2> array;
  SO3(const int64_t &a, const int64_t &b):
    array({a,b}){}

  int64_t operator[](const int64_t &i) const
  {
    return array[i];
  }

  bool operator<(const SO3 &so3) const
  {
    return array < so3.array;
  }
};

inline std::ostream &operator<<(std::ostream &os, const SO3 &so3)
{
  return os << so3.array;
}
