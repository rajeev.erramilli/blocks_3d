#include "Q.hxx"
#include "../../two_j_values.hxx"
#include "../../../../Cache.hxx"

#include <functional>
#include <deque>

void set_residue(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &r_order_index, const size_t &two_j_index,
  const size_t &pole_index, const int64_t &r_shift,
  const size_t &two_j_prime_index,
  const std::vector<std::reference_wrapper<Bigfloat>> &delta_vector,
  const size_t &parity_index, const size_t &source_parity_index,
  const std::vector<std::vector<Bigfloat>> &LM,
  const std::vector<std::vector<Bigfloat>> &RM, const Bigfloat &Q_val,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues);

namespace
{
  inline const std::vector<std::vector<Bigfloat>> &
  L_Matrix(const Pole &pole, const int64_t &two_j12,
           const int64_t &prime_pseudo_parity, const int64_t &two_j,
           Cache &cache)
  {
    return cache.m_matrix_eval(pole, M_Matrix::Delta::delta_12, two_j12,
                               prime_pseudo_parity, two_j);
  }

  inline std::vector<std::vector<Bigfloat>>
  R_Matrix(const Pole &pole, const int64_t &two_j43,
           const int64_t &prime_pseudo_parity, const int64_t &two_j,
           Cache &cache)
  {
    std::vector<std::vector<Bigfloat>> result(cache.m_matrix_eval(
      pole, M_Matrix::Delta::delta_43, two_j43, prime_pseudo_parity, two_j));
    if((pole.two_j(two_j) - two_j) % 4 != 0)
      {
        for(auto &vec : result)
          for(auto &element : vec)
            {
              element = -element;
            }
      }
    return result;
  }
}

void fill_residues(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, Cache &cache,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues)
{
  // Some complications because we precompute the possible elements of
  // delta_vector and just use the correct reference.
  // Use deque because references to elements are not invalidated when
  // adding things to the end.
  std::deque<Bigfloat> inverse_positive(1, 0), inverse_negative(1, 0);
  Q Q_vals;
  for(int64_t r_order_index(0); r_order_index != order; ++r_order_index)
    {
      std::vector<int64_t> two_js(
        two_j_values(r_order_index + 1, order, two_j12, two_j_max));

      for(size_t two_j_index(0); two_j_index != two_js.size(); ++two_j_index)
        {
          const int64_t two_j(two_js[two_j_index]);
          const std::vector<Pole> poles(
            cache.pole_types.eval(two_j, two_j12, two_j43, order));

          for(size_t pole_index(0); pole_index != poles.size(); ++pole_index)
            {
              const Pole pole(poles[pole_index]);
              const int64_t r_shift(pole.shift());
              if(r_shift <= r_order_index + 1)
                {
                  const int64_t two_j_prime(pole.two_j(two_j));
                  std::vector<std::reference_wrapper<Bigfloat>> delta_vector;
                  {
                    const int64_t delta_2_prime(pole.delta_2(two_j)
                                                + 2 * r_shift);
                    const std::vector<Pole> prime_poles(cache.pole_types.eval(
                      two_j_prime, two_j12, two_j43, order));

                    delta_vector.reserve(prime_poles.size());
                    for(size_t prime_pole_index(0);
                        prime_pole_index != prime_poles.size();
                        ++prime_pole_index)
                      {
                        const int64_t prime_pole_delta_2(
                          prime_poles[prime_pole_index].delta_2(two_j_prime));

                        const size_t diff(
                          std::abs(delta_2_prime - prime_pole_delta_2));
                        if(delta_2_prime >= prime_pole_delta_2)
                          {
                            const size_t old_size(inverse_positive.size());
                            for(size_t index(old_size); index <= diff; ++index)
                              {
                                inverse_positive.emplace_back(2);
                                inverse_positive.back() /= index;
                              }
                            delta_vector.emplace_back(inverse_positive[diff]);
                          }
                        else
                          {
                            const size_t old_size(inverse_negative.size());
                            for(size_t index(old_size); index <= diff; ++index)
                              {
                                inverse_negative.emplace_back(-2);
                                inverse_negative.back() /= index;
                              }
                            delta_vector.emplace_back(inverse_negative[diff]);
                          }
                      }
                  }
                  const size_t two_j_prime_index(two_j_prime / 2);
                  for(size_t parity_index(0); parity_index != 2;
                      ++parity_index)
                    {
                      const size_t source_parity_index(
                        (parity_index + pole.parity()) % 2);

                      const std::vector<std::vector<Bigfloat>> LM(L_Matrix(
                        pole, two_j12, pseudo_parities[source_parity_index][0],
                        two_j, cache)),
                        RM(R_Matrix(pole, two_j43,
                                    pseudo_parities[source_parity_index][1],
                                    two_j, cache));
                      const Bigfloat &Q_val(Q_vals.eval(pole, two_j));

                      set_residue(
                        h_infinity, r_order_index, two_j_index, pole_index,
                        r_shift, two_j_prime_index, delta_vector, parity_index,
                        source_parity_index, LM, RM, Q_val, residues);
                    }
                }
            }
        }
    }
}
