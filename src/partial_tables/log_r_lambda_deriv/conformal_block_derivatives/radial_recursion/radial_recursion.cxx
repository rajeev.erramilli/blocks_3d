#include "../../../Cache.hxx"

#include <vector>
#include <array>

void zero_init_residues(
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, Pole_Types &pole_types,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues);

void fill_residues(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, Cache &cache,
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    &residues);

// This function performs radial recursion given the vectorized
// hInfinity and other standard parameters.

// pseudoparities = {{pL1, pR1},{pL2,pR2}}

// This tells us which pseudoparities to use. When converted to
// parities (depends on external spins which are not an input),
// this becomes {{0,0},{1,1}} or {{0,1},{1,0}} depending on the parity
// of the 4-pt structure.

// Valid input has

// twojMax,twoj12,twoj43,order >= 0
// twojMax-twoj12 and twojMax-twoj43 are even

// pseudo_parities = {{pL1, pR1},{pL2,pR2}} with all entries being 0
// or 1. Furthermore,
// pseudo_parities[0]+1 % 2 == pseudo_parities[1]

std::vector<
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
radial_recursion(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, Cache &cache)
{
  std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    result(2); // 2 parities

  zero_init_residues(two_j_max, two_j12, two_j43, pseudo_parities, order,
                     cache.pole_types, result);

  fill_residues(h_infinity, two_j_max, two_j12, two_j43, pseudo_parities,
                order, cache, result);
  return result;
}
