#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"
#include "../../../Single_Spin.hxx"

#include <set>

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset);

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<Bigfloat> &all_kept_first,
  const std::vector<Bigfloat> &all_kept_second,
  const boost::math::tools::polynomial<Bigfloat> &polynomial, Cache &cache);

namespace
{
  bool is_protected(const Bigfloat &p)
  {
    // 100 seems big enough to account for any rounding errors
    return boost::multiprecision::abs(p)
           <= std::numeric_limits<Bigfloat>::epsilon() * 100;
  }
}

Single_Spin shift_block(const std::vector<Delta_Fraction> &block,
                        const int64_t &two_j, const int64_t &two_j12,
                        const int64_t &two_j43, const int64_t &kept_pole_order,
                        std::set<Bigfloat> &output_poles, Cache &cache)
{
  const std::vector<Pole> &poles(
    cache.pole_types.eval(two_j, two_j12, two_j43, kept_pole_order));
  std::vector<Bigfloat> keep;

  Single_Spin result;
  for(auto &pole : poles)
    {
      const Bigfloat pole_to_keep(
        pole.delta(two_j)
        - (two_j <= 1 ? two_j * 0.5 + 0.5 : two_j * 0.5 + 1));
      if(!is_protected(pole_to_keep))
        {
          keep.push_back(pole_to_keep);
        }
    }
  for(auto &fraction : block)
    {
      const Delta_Fraction shifted(shift_fraction(
        fraction, (two_j <= 1 ? two_j * 0.5 + 0.5 : two_j * 0.5 + 1)));
      std::vector<Bigfloat> all_kept_first, all_kept_second;
      std::vector<std::pair<Bigfloat, Bigfloat>> unprotected_poles;
      for(auto &residue : shifted.residues)
        {
          if(is_protected(residue.first))
            {
              all_kept_first.push_back(residue.first);
              all_kept_second.push_back(residue.second);
            }
          else
            {
              unprotected_poles.push_back(residue);
            }
        }
      const std::vector<std::pair<Bigfloat, Bigfloat>> &shifted_poles(
        cache.shift_poles_eval(unprotected_poles, keep));
      for(auto &pole : shifted_poles)
        {
          all_kept_first.push_back(pole.first);
          all_kept_second.push_back(pole.second);
        }

      for(auto &kept : all_kept_first)
        {
          output_poles.insert(kept);
        }
      result.numerator.emplace_back(together_with_factors(
        all_kept_first, all_kept_second, shifted.polynomial, cache));
    }
  return result;
}
