#pragma once

#include "../Eigen.hxx"

#include <map>
#include <vector>

struct Keep_LU
{
  std::map<std::vector<Bigfloat>, Eigen::PartialPivLU<Eigen_Matrix>> cache;

  const Eigen::PartialPivLU<Eigen_Matrix> &
  eval(const std::vector<Bigfloat> &keep)
  {
    auto element(cache.find(keep));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(keep);
      }
  }
  const Eigen::PartialPivLU<Eigen_Matrix> &
  calculate(const std::vector<Bigfloat> &keep);
};
