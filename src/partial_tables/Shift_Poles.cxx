#include "Shift_Poles.hxx"
#include "../Eigen.hxx"

const std::vector<std::pair<Bigfloat, Bigfloat>> &Shift_Poles::calculate(
  const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
  const std::vector<Bigfloat> &keep, Keep_LU &keep_lu)
{
  const int64_t size(keep.size());
  Bigfloat temp;
  Eigen_Vector b(Eigen_Vector::Zero(size));
  for(auto &pole : unprotected_poles)
    {
      temp = pole.second;
      temp *= pow(pole.first, -(size / 2));
      for(int64_t row = 0; row < size; ++row)
        {
          b(row) += temp; // pole.second * pole.first ^ (row - (size / 2))
          if(row != size - 1)
            {
              temp *= pole.first;
            }
        }
    }

  Eigen_Vector x = keep_lu.eval(keep).solve(b);
  std::tuple<std::vector<std::pair<Bigfloat, Bigfloat>>, std::vector<Bigfloat>>
    key(unprotected_poles, keep);
  auto iterator(
    (cache.emplace(key, std::vector<std::pair<Bigfloat, Bigfloat>>())).first);
  std::vector<std::pair<Bigfloat, Bigfloat>> &result(iterator->second);
  result.reserve(size);
  for(int64_t row = 0; row < size; ++row)
    {
      result.emplace_back(keep[row], x(row));
    }
  return result;
}
