#include "Clebsch_Gordan.hxx"

const Bigfloat &
Clebsch_Gordan::calculate(const int64_t &two_j12, const int64_t &two_j120,
                          const int64_t &two_j, const int64_t &two_m,
                          Factorial &factorial)
{
  std::tuple<int64_t, int64_t, int64_t, int64_t> key(two_j12, two_j120, two_j,
                                                     two_m);
  auto iterator((cache.emplace(key, Bigfloat())).first);
  Bigfloat &result(iterator->second);

  if(two_m > 0)
    {
      result = (((two_j12 + two_j - two_j120) % 4 == 0 ? 1 : -1)
                * eval(two_j12, two_j120, two_j, -two_m, factorial));
    }
  else
    {
      result = 0;
      if((two_j120 - two_j12 - two_j) % 2 == 0
         && std::abs(two_j12 - two_j) <= two_j120
         && two_j120 <= two_j12 + two_j && (two_j120) % 2 == 0
         && (two_j12 - two_m) % 2 == 0 && (two_j - two_m) % 2 == 0
         && std::abs(two_m) <= two_j12 && std::abs(two_m) <= two_j)
        {
          int64_t two_s(two_j120 - two_j);
          result = sqrt(Pochhammer((two_j + two_m) / 2 + 1, -two_m)
                        * factorial.eval((two_j12 - two_m) / 2)
                        * factorial.eval((two_j12 + two_m) / 2))
                   * sqrt(((1 + two_j + two_s)
                           * factorial.eval((two_j12 - two_s) / 2)
                           * factorial.eval((two_j12 + two_s) / 2))
                          / Pochhammer(two_j - (two_j12 - two_s) / 2 + 1,
                                       two_j12 + 1));
          Bigfloat sum(0);
          for(int64_t k(
                std::max(int64_t(0), std::max(two_j - two_j120 + two_m,
                                              two_j12 - two_j120 + two_m))
                / 2);
              k
              <= std::min(two_j12 + two_m,
                          std::min(two_j + two_m, two_j + two_j12 - two_j120))
                   / 2;
              ++k)
            {
              sum += (k % 2 == 0 ? 1 : -1)
                     * Pochhammer((two_j + two_m) / 2 - k + 1, k)
                     * Pochhammer((-two_j12 + two_j120 - two_m) / 2 + k + 1,
                                  (two_j12 + two_m) / 2 - k)
                     * (factorial.inverse_eval((two_j + two_j12 - two_j120) / 2
                                               - k)
                        * factorial.inverse_eval(k)
                        * factorial.inverse_eval(
                            (-two_j + two_j120 - two_m) / 2 + k)
                        * factorial.inverse_eval((two_j12 + two_m) / 2 - k));
            }
          result *= sum;
        }
    }
  return result;
}
