#include "Pole_Product.hxx"

const boost::math::tools::polynomial<Bigfloat> &
Pole_Product::calculate(const std::vector<Bigfloat> &all_kept)
{
  boost::math::tools::polynomial<Bigfloat> pole_product({1});
  for(auto &pole : all_kept)
    {
      pole_product *= boost::math::tools::polynomial<Bigfloat>({-pole, 1});
    }
  return ((cache.emplace(all_kept, pole_product)).first)->second;
}
