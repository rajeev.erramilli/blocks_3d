#include "../../M_Matrix.hxx"
#include "../../../so3_tensor_parity.hxx"

Bigfloat
M(const Pole &pole, const Bigfloat &delta_NN, const int64_t &two_jNN,
  const int64_t &two_jNN0, const int64_t &two_jNN0p, const int64_t &two_j,
  Clebsch_Gordan &clebsch_gordan, Factorial &factorial);

std::vector<std::vector<Bigfloat>> &
M_Matrix::calculate(const Pole &pole, const M_Matrix::Delta &delta_NN,
                    const Bigfloat &delta, const int64_t &two_jNN,
                    const int64_t &prime_pseudo_parity, const int64_t &two_j,
                    Clebsch_Gordan &clebsch_gordan, Factorial &factorial)
{
  std::vector<int64_t> structures(so3_tensor_parity(
    (prime_pseudo_parity + pole.parity()) % 2, two_jNN, two_j)),
    prime_structures(
      so3_tensor_parity(prime_pseudo_parity, two_jNN, pole.two_j(two_j)));

  std::tuple<Pole, M_Matrix::Delta, int64_t, int64_t, int64_t> key(
    pole, delta_NN, two_jNN, prime_pseudo_parity, two_j);
  auto iterator(
    (cache.emplace(key, std::vector<std::vector<Bigfloat>>())).first);
  std::vector<std::vector<Bigfloat>> &result(iterator->second);
  result.resize(structures.size());

  auto vec(result.begin());
  for(auto &two_jNN0 : structures)
    {
      vec->reserve(prime_structures.size());
      for(auto &two_jNN0p : prime_structures)
        {
          vec->emplace_back(M(pole, delta, two_jNN, two_jNN0, two_jNN0p, two_j,
                              clebsch_gordan, factorial));
        }
      ++vec;
    }
  return iterator->second;
}
