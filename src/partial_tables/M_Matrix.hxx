#pragma once

#include "Pole.hxx"
#include "Clebsch_Gordan.hxx"

#include <map>
#include <tuple>

struct M_Matrix
{
  // We use an enum here because comparing two Bigfloat's is slow
  enum class Delta : int8_t
  {
    delta_12,
    delta_43
  };
  Bigfloat delta_12, delta_43;

  std::map<std::tuple<Pole, Delta, int64_t, int64_t, int64_t>,
           std::vector<std::vector<Bigfloat>>>
    cache;

  M_Matrix(const Bigfloat &Delta_12, const Bigfloat &Delta_43)
      : delta_12(Delta_12), delta_43(Delta_43)
  {}
  std::vector<std::vector<Bigfloat>> &
  eval(const Pole &pole, const Delta &delta_NN, const int64_t &two_jNN,
       const int64_t &prime_pseudo_parity, const int64_t &two_j,
       Clebsch_Gordan &clebsch_gordan, Factorial &factorial)
  {
    std::tuple<Pole, Delta, int64_t, int64_t, int64_t> key(
      pole, delta_NN, two_jNN, prime_pseudo_parity, two_j);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        if(delta_NN == Delta::delta_12)
          {
            return calculate(pole, delta_NN, delta_12, two_jNN,
                             prime_pseudo_parity, two_j, clebsch_gordan,
                             factorial);
          }
        return calculate(pole, delta_NN, delta_43, two_jNN,
                         prime_pseudo_parity, two_j, clebsch_gordan,
                         factorial);
      }
  }
  std::vector<std::vector<Bigfloat>> &
  calculate(const Pole &pole, const Delta &delta_NN, const Bigfloat &delta,
            const int64_t &two_jNN, const int64_t &prime_pseudo_parity,
            const int64_t &two_j, Clebsch_Gordan &clebsch_gordan,
            Factorial &factorial);
};
