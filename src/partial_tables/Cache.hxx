#pragma once

#include "Factorial.hxx"
#include "Clebsch_Gordan.hxx"
#include "M_Matrix.hxx"
#include "Pole_Types.hxx"
#include "Shift_Poles.hxx"
#include "Keep_LU.hxx"
#include "Sinh_Power_Derivative.hxx"
#include "Pole_Product.hxx"
#include "Pole_Complement_Product.hxx"

struct Cache
{
  Clebsch_Gordan clebsch_gordan;
  M_Matrix m_matrix;
  Pole_Types pole_types;
  Shift_Poles shift_poles;
  Keep_LU keep_lu;
  Factorial factorial;
  Sinh_Power_Derivative sinh_power_derivative;
  Pole_Product pole_product;
  Pole_Complement_Product pole_complement_product;

  Cache(const Bigfloat &delta_12, const Bigfloat &delta_43)
      : m_matrix(delta_12, delta_43)
  {}

  const std::vector<std::vector<Bigfloat>> &
  m_matrix_eval(const Pole &pole, const M_Matrix::Delta &delta_NN,
                const int64_t &two_jNN, const int64_t &prime_pseudo_parity,
                const int64_t &two_j)
  {
    return m_matrix.eval(pole, delta_NN, two_jNN, prime_pseudo_parity, two_j,
                         clebsch_gordan, factorial);
  }

  const Bigfloat &
  clebsch_gordan_eval(const int64_t &two_j12, const int64_t &two_j120,
                      const int64_t &two_j, const int64_t &two_m)
  {
    return clebsch_gordan.eval(two_j12, two_j120, two_j, two_m, factorial);
  }

  const std::vector<std::pair<Bigfloat, Bigfloat>> &shift_poles_eval(
    const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
    const std::vector<Bigfloat> &keep)
  {
    return shift_poles.eval(unprotected_poles, keep, keep_lu);
  }
};
