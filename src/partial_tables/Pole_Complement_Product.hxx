#pragma once

#include "../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <map>
#include <vector>

struct Pole_Complement_Product
{
  std::map<std::vector<Bigfloat>,
           std::vector<boost::math::tools::polynomial<Bigfloat>>>
    cache;

  const std::vector<boost::math::tools::polynomial<Bigfloat>> &
  eval(const std::vector<Bigfloat> &all_kept)
  {
    auto element(cache.find(all_kept));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(all_kept);
      }
  }
  const std::vector<boost::math::tools::polynomial<Bigfloat>> &
  calculate(const std::vector<Bigfloat> &all_kept);
};
