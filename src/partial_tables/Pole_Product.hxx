#pragma once

#include "../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <map>
#include <vector>

struct Pole_Product
{
  std::map<std::vector<Bigfloat>, boost::math::tools::polynomial<Bigfloat>>
    cache;

  const boost::math::tools::polynomial<Bigfloat> &
  eval(const std::vector<Bigfloat> &all_kept)
  {
    auto element(cache.find(all_kept));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(all_kept);
      }
  }
  const boost::math::tools::polynomial<Bigfloat> &
  calculate(const std::vector<Bigfloat> &all_kept);
};
