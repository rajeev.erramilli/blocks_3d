#include "Transforms.hxx"
#include "../Single_Spin.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
xt_to_ws_radial(
  const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &xt_radial)
{
  auto &xt_front(xt_radial.front());
  const int64_t order(xt_front.size()-1);
  const Eigen_Matrix &transform(transforms.get(order, Transforms::Index::z_y));

  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(1);
  result.front().reserve(xt_front.size());
  for(size_t row(0); row!=xt_front.size(); ++row)
    {
      result.front().emplace_back();
      auto &temp(result.front().back());
      for(size_t column(0); column!=xt_front.size(); ++column)
        {
          temp+=transform(row,column)*xt_front[column];
        }
    }
  return result;
}
