#include "Transforms.hxx"
#include "../Single_Spin.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_to_xt_radial(const int64_t &parity, const Transforms &transforms,
                  const Single_Spin &spin)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(1);
  const size_t spin_size(spin.numerator.size());
  result[0].reserve(spin_size);

  const int64_t order(spin_size - 1);
  const Eigen_Matrix &stirling(
    transforms.get(order, Transforms::Index::stirling)),
    &rho_z(transforms.get(order, Transforms::Index::rho_z)),
    &radial_prefactor(
      parity == 0
        ? transforms.get(order, Transforms::Index::radial_power_prefactor)
        : transforms.get(order,
                         Transforms::Index::radial_one_minus_power_product));

  const Eigen_Matrix total_transform(radial_prefactor * rho_z * stirling);
  for(size_t ii(0); ii < spin_size; ++ii)
    {
      result[0].emplace_back();
      auto &element(result[0].back());
      for(size_t jj(0); jj < spin_size; ++jj)
        {
          element += spin.numerator[jj] * total_transform(ii, jj);
        }
    }
  return result;
}
