#include "../Bigfloat.hxx"
#include "../Coordinate.hxx"

#include <boost/filesystem/fstream.hpp>
#include <boost/math/tools/polynomial.hpp>

void write_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &derivs,
  boost::filesystem::ofstream &outfile)
{
  // boost::epsilon is wrong, so we need compute it ourselves from the
  // precision.

  // Multiply by 0.9 to include some variation from the absolute
  // smallest number.
  const int64_t scaled_precision(0.9 * log2(10)
                                 * Bigfloat::default_precision());
  Bigfloat epsilon(0.5);
  mpf_div_2exp(epsilon.backend().data(), epsilon.backend().data(),
               scaled_precision);

  outfile << "          [\n";
  for(size_t deriv_0(0); deriv_0 < derivs.size(); ++deriv_0)
    {
      outfile << "            [\n";
      for(size_t deriv_1(0); deriv_1 < derivs[deriv_0].size(); ++deriv_1)
        {
          outfile << "              [\n";
          if(derivs[deriv_0][deriv_1].is_zero())
            {
              outfile << "\"0\"";
            }
          else
            {
              Bigfloat max_size(0);
              for(auto &coeff : derivs[deriv_0][deriv_1].data())
                {
                  max_size = max(max_size, abs(coeff));
                }
              const Bigfloat cutoff(max_size * epsilon);
              size_t truncated_size(derivs[deriv_0][deriv_1].size());
              while(truncated_size > 1
                    && abs(derivs[deriv_0][deriv_1][truncated_size - 1])
                         < cutoff)
                {
                  --truncated_size;
                }
              for(size_t polynomial_coeff(0);
                  polynomial_coeff < truncated_size; ++polynomial_coeff)
                {
                  if(polynomial_coeff != 0)
                    {
                      outfile << ",\n";
                    }
                  outfile << "\"" << derivs[deriv_0][deriv_1][polynomial_coeff]
                          << "\"";
                }
            }
          outfile << "\n              ]";
          if(deriv_1 + 1 != derivs[deriv_0].size())
            {
              outfile << ",\n";
            }
        }
      outfile << "\n            ]";
      if(deriv_0 + 1 != derivs.size())
        {
          outfile << ",\n";
        }
    }
  outfile << "\n          ]";
}
