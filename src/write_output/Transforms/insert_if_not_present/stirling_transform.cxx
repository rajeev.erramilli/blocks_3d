#include "../../../Eigen.hxx"

Eigen_Matrix stirling_transform(const int64_t &order)
{
  Eigen_Matrix result(order + 1, order + 1);
  Eigen_Vector prefactor(order + 1);

  for(int64_t n(0); n <= order; ++n)
    {
      prefactor(n) = pow(3 - 2 * sqrt(Bigfloat(2.0)), -n);
    }

  Eigen_Matrix stirling(order + 1, order + 1);
  stirling(0, 0) = 1;
  for(int64_t n(0); n < order; ++n)
    for(int64_t k(0); k <= n; ++k)
      {
        stirling(n + 1, k + 1) = -n * stirling(n, k + 1) + stirling(n, k);
      }
  return prefactor.asDiagonal() * stirling;
}
