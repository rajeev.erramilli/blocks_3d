#pragma once

#include "../Eigen.hxx"

#include <map>
#include <array>

struct Transforms
{
  enum class Index
  {
    stirling,
    rho_z,
    z_y,
    power_prefactor,
    one_minus_power_product,
    radial_power_prefactor,
    radial_one_minus_power_product
  };

  std::map<int64_t, std::array<Eigen_Matrix, 7>> map;
  Bigfloat Delta;

  bool needs_prefactor() const { return !Delta.is_zero(); }

  Transforms(const Bigfloat &delta) : Delta(delta) {}

  const Eigen_Matrix &get(const int64_t &order, const Index &index) const
  {
    auto iterator(map.find(order));
    if(iterator == map.end())
      {
        throw std::runtime_error(
          "INTERNAL ERROR: Unable to find transform for order="
          + std::to_string(order)
          + ", index=" + (index == Index::stirling ? "stirling" : "rho_z"));
      }
    switch(index)
      {
      case Index::stirling: return iterator->second[0]; break;
      case Index::rho_z: return iterator->second[1]; break;
      case Index::z_y: return iterator->second[2]; break;
      case Index::power_prefactor: return iterator->second[3]; break;
      case Index::one_minus_power_product: return iterator->second[4]; break;
      case Index::radial_power_prefactor: return iterator->second[5]; break;
      case Index::radial_one_minus_power_product:
        return iterator->second[6];
        break;
      default:
        throw std::runtime_error(
          "INTERNAL ERROR: unhandled case in Transforms::get");
      }
  }

  void insert_if_not_present(const int64_t &order);
};
